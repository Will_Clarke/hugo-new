using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class KillSceneScript : MonoBehaviour
{

    public GameObject trigger;
    public GameObject Enemy;
    public GameObject Hugo;
    public GameObject Enemy1;
    public GameObject Enemy2;
    public GameObject Camera;
    public GameObject MainMenu;
    public GameObject BlackScreen;
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            StartCoroutine(KillScene());

        }
        
    }

    IEnumerator KillScene()
    {
        Enemy.SetActive(false);
        Hugo.SetActive(false);

        Enemy1.SetActive(true);
        Enemy2.SetActive(true);
        trigger.SetActive(false);
        Camera.SetActive(true);
        MainMenu.SetActive(true);
        yield return new WaitForSeconds(2f);
       
       

    }
}
