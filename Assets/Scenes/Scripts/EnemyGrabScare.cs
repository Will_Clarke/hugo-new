using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGrabScare : MonoBehaviour
{
    public Transform moveTo;
    public GameObject Enemy;
    public GameObject Trigger;
    public GameObject BlackScreenIn;

    public int speed = 5;


    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.tag == "Player")
        {
            StartCoroutine(LightsOff());
            
        }
       
    }

    IEnumerator LightsOff()
    {
        Trigger.GetComponent<Collider>().isTrigger = false;
        BlackScreenIn.SetActive(true);
        FindObjectOfType<AudioManager>().Play("TorchScare");
        yield return new WaitForSeconds(0.2f);

        BlackScreenIn.SetActive(false);
        Debug.Log("hello");
        Enemy.transform.position = new Vector3(91, 5, -58);
        yield return new WaitForSeconds(0.03f);
        //chair.transform.Translate (Vector3.right* speed * Time.deltaTime);
        FindObjectOfType<AudioManager>().Play("EnemyScream");
        yield return new WaitForSeconds(1f);
        Enemy.transform.position = new Vector3(92, 5, -58);
        yield return new WaitForSeconds(0.7f);
        Enemy.transform.position = new Vector3(93, 5, -58);
        yield return new WaitForSeconds(0.7f);
        Enemy.transform.position = new Vector3(94, 5, -58);
        yield return new WaitForSeconds(0.7f);
        Enemy.SetActive(false);
    }

}
