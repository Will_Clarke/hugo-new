using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEndMenu : MonoBehaviour
{
    public GameObject Options;
    public GameObject Main;
    public void PlayGameMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    public void QuitGame()
    {
        Debug.Log("quit");
        Application.Quit();
    }

    public void Back()
    {
        Options.SetActive(false);
        Main.SetActive(true);

    }
}
