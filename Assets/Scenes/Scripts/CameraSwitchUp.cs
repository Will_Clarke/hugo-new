using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitchUp : MonoBehaviour
{
    public GameObject cameraSwitch;
    public GameObject wallblock;
    

    private void OnTriggerEnter(Collider other)
    {
        cameraSwitch.GetComponent<CameraFollowTrigger>().enabled = false;
        cameraSwitch.transform.position = new Vector3(86.45f, 12, -86);
        cameraSwitch.transform.eulerAngles = new Vector3(0f, 270, 0);
        wallblock.SetActive(true);
    }
    
}
    
