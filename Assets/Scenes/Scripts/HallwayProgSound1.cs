using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayProgSound1 : MonoBehaviour
{
    // Start is called before the first frame update
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.tag == "Player")
        {

            FindObjectOfType<AudioManager>().Play("prog1");
            FindObjectOfType<AudioManager>().Play("HallAmb1");
        }

    }
}
