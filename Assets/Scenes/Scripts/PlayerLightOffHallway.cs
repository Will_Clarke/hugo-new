using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLightOffHallway : MonoBehaviour
{
    public GameObject playerLight;
    public GameObject stationLight;
    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        
        playerLight.SetActive(false);
        stationLight.SetActive(false);

    }
}
