using UnityEngine;
using System.Collections;

public class CameraFollowTrigger : MonoBehaviour
{

    public GameObject player;

    // Start is called before the first frame update
    void Start()
    {
         // The player
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y + 4, player.transform.position.z - -7);
    }
}