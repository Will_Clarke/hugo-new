using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint2 : MonoBehaviour
{
    private GameMaster gm;
    public GameObject Camera1;
    Animation anim;
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GM").GetComponent<GameMaster>();

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            gm.lastCheckPointPos = transform.position;
            Camera1.GetComponent<Animation>().enabled = false;
            Camera1.SetActive(false);
            
        }
    }
}
