using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchFlashTrigger : MonoBehaviour
{
    public GameObject LightBeam;
    public GameObject GlassReflection;
    public GameObject ColliderTrigger;

    private void OnTriggerEnter(Collider other)
    {
        LightBeam.GetComponent<Animation>().Play("TorchTriggerFlash");
        GlassReflection.GetComponent<Animation>().Play("GlassTriggerFlash");
        ColliderTrigger.GetComponent<AudioSource>().enabled = true;
    }
    private void OnTriggerExit(Collider other)
    {
        LightBeam.GetComponent<Animation>().Stop("TorchTriggerFlash");
        GlassReflection.GetComponent<Animation>().Stop("GlassTriggerFlash");
        ColliderTrigger.GetComponent<AudioSource>().enabled = false;
        this.GetComponent<BoxCollider>().enabled = false;
    }
}
