using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour
{

    public GameObject cameraOld;
    public GameObject caneraNew;
    public GameObject playerLight;
    public GameObject stationLight;
    public GameObject player;
    
    // Start is called before the first frame update
    public void Awake()
    {
        //playerLight = GetComponent<Light>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        cameraOld.SetActive(false);
        caneraNew.SetActive(true);
        playerLight.SetActive(false);
        stationLight.SetActive(true);
        
       
    }
}
