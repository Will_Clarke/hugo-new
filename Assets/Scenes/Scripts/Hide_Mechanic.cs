using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hide_Mechanic : MonoBehaviour
{
    public float TheDistance;
    public GameObject ActionDisplay;
    public GameObject ActionText;
    public GameObject HidingPlace;
    public AudioSource HideCreakSound;
    public GameObject player;

    private void Update()
    {
        TheDistance = PlayerCasting.DistanceFromTarget;

    }

    private void OnTriggerEnter()
    {
        if(TheDistance <= 2)
        {
            ActionDisplay.SetActive(true);
            ActionText.SetActive(true);

        }
        if(Input.GetButtonDown("Action"))
        {
            if(TheDistance <= 2)
            {
                ActionDisplay.SetActive(false);
                ActionText.SetActive(false);
            }

            
        }
    }

    void OnTriggerExit()
    {
        ActionDisplay.SetActive(false);
        ActionText.SetActive(false);

    }
}