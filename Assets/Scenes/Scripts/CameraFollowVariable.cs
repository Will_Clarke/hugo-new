using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowVariable : MonoBehaviour
{
    public GameObject player;
    public GameObject cameraFollow;
    public GameObject oldtrigger;
    public GameObject enemy;
    public GameObject Path;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(CameraChangeFollow());
    }

    IEnumerator CameraChangeFollow()
    {
        cameraFollow.transform.position = new Vector3(1, 15, -52);
        cameraFollow.transform.eulerAngles = new Vector3(0f, 180, 0);
        
        yield return new WaitForSeconds(1f);
        
    }
 



}
