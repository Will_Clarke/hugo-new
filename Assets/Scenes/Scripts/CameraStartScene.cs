using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStartScene : MonoBehaviour
{

	
	public GameObject The_Cam;
	public GameObject Player;


	void Start()
	{
		
		
			StartCoroutine(CameraAnimation());
		

	}

	IEnumerator CameraAnimation()
	{
		
		{
			The_Cam.GetComponent<CameraFollow>().enabled = false;
			Player.GetComponent<PlayerMovement>().enabled = false;
			The_Cam.GetComponent<Animation>().Play("CameraDropStart");

		}
		yield return new WaitForSeconds(3f);

		The_Cam.GetComponent<CameraFollow>().enabled = true;
		Player.GetComponent<PlayerMovement>().enabled = true;
	}
}








