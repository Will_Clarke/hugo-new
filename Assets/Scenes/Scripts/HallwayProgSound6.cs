using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayProgSound6 : MonoBehaviour
{
    void OnTriggerEnter(Collider col)
    {

        if (col.gameObject.tag == "Player")
        {

            FindObjectOfType<AudioManager>().Play("prog6");
        }

    }
}
