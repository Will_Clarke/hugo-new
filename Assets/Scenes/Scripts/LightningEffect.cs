using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningEffect : MonoBehaviour
{
	public int LightMode;
	public GameObject Lightning;


	void Update()
	{
		if (LightMode == 0)
		{
			StartCoroutine(AnimateLight());
		}

	}

	IEnumerator AnimateLight()
	{
		LightMode = Random.Range(1, 4);
		if (LightMode == 1)
		{
			Lightning.GetComponent<Animation>().Play("Lightning1");
			FindObjectOfType<AudioManager>().Play("Lightning1");
		}
		if (LightMode == 2)
		{
			Lightning.GetComponent<Animation>().Play("Lightning2");
			FindObjectOfType<AudioManager>().Play("Lightning2");
		}
		if (LightMode == 3)
		{
			Lightning.GetComponent<Animation>().Play("Lightning3");
			FindObjectOfType<AudioManager>().Play("Lightning3");
		}
		yield return new WaitForSeconds(15f);
		LightMode = 0;

	}
}

