using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorchTrigger : MonoBehaviour
{
    public GameObject LightBeam;
    public GameObject GlassReflection;
    public GameObject TorchCollider;


    private void OnTriggerEnter(Collider other)
    {
        LightBeam.GetComponent<Light>().enabled = false;
        GlassReflection.GetComponent<Light>().enabled = false;
        FindObjectOfType<AudioManager>().Play("TorchScare2");
        FindObjectOfType<AudioManager>().Play("TorchScare");
        TorchCollider.GetComponent<AudioSource>().enabled = false;
        TorchCollider.GetComponent<BoxCollider>().enabled = false;
    }
}
